require '../lib/CatBox.rb'
cache=CatBox.new('cms_plugin','localhost','root','root')
puts "Enter write:key:string read:key exit"
input = gets.chomp
while input!="exit"
	input=input.split(":")
	if input[0]=="write" && !input[1].nil? && !input[2].nil?
		puts "Writing:#{input[1].to_sym}:#{input[2]}"
		cache.put(input[1].to_sym, input[2])
	elsif input[0]=="read" && !input[1].nil?
		puts "Reading:#{input[1].to_sym}:#{cache.fetch(input[1].to_sym)}"
	end
	puts "Enter write:key:string read:key exit"
	input = gets.chomp
end