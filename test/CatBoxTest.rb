require 'CatBox.rb'

class TestSuite
	def initialize
		puts "Starting CatBox Tests"
	end
	
	def test_line(test_bool)
		if test_bool
			print "."
		else
			print "X"
			@fail=true
		end
		STDOUT.flush
	end
	
	def test_start
		@fail=false
	end
	
	def test_end
		if @fail
			print "FAIL!\n" 
		else
			print "PASS!\n" 
		end
		STDOUT.flush
	end

	def test_data(data1,data2)
		if data1['data'][0]==data2['data'][0] && data1['data'][1]==data2['data'][1] && data1['key']==data2['key']
			return true 
		else
			return false
		end
	end

	def test_read_write
		test_start
		print "Testing Read and Write"
		data=[]
		iterations=10
		(0..iterations).each do |x|
			key = ""; 8.times{key  << (65 + rand(25)).chr}
			value = ""; 8.times{value  << (65 + rand(25)).chr}
			data<<[key,{'key'=>key,'data'=>[x,value]}]
		end
		cache=CatBox.new('cms_plugin','localhost','root','root')
		cache.empty_cache
		(0..iterations).each do |i|
			test_line cache.put(data[i][0], data[i][1])
		end
		(0..iterations).each do |i|
			test_line test_data(data[i][1],cache.fetch(data[i][0]))
		end
		test_end
	end

	def performance_test(iterations=100)
		data=[]
		(0..iterations).each do |x|
			key = ""; 8.times{key  << (65 + rand(25)).chr}
			value = ""; 8.times{value  << (65 + rand(25)).chr}
			data<<[key,value]
		end
		cache=CatBox.new('cms_plugin','localhost','root','root')
		cache.empty_cache
		print "Write Performance Test(#{iterations})"
		start_time=Time.now.to_f
			(0..iterations).each do |i|
				cache.put(data[i][0], data[i][1])
			end
		end_time=Time.now.to_f
		print "...\n   Total Time:#{end_time-start_time}\n   Writes Per Second:#{iterations/(end_time-start_time)}\n   Time per Write:#{(end_time-start_time)/iterations}\n"
		print "Read Performance Test(#{iterations})"
		start_time=Time.now.to_f
			(0..iterations).each do |i|
				cache.fetch(data[i][0])
			end
		end_time=Time.now.to_f
		print "...\n   Total Time:#{end_time-start_time}\n   Reads Per Second:#{iterations/(end_time-start_time)}\n   Time per Read:#{(end_time-start_time)/iterations}\n"
		cache.empty_cache
	end
end