require "msgpack"
require "mysql2"
# @author Christian Stone
#Main CatBox Class
#Used for Storing Key Value Data to a MySql table
class CatBox
	# Initializes CatBox by creating DB Connection and Creating CatBox table if needed
	# @param [String] scheme the database to connect to
	# @param [String] host the IP address of MySql Server
	# @param [String] user the MySql user on the server
	# @param [String, nil] password the password of the user
	# @return [Boolean] True if All Goes Well
	def initialize(scheme=nil,host="127.0.0.1",user="admin",password=nil)
		options={:database => scheme,:host =>host, :username => user}
		options[:password]=password if !password.nil?
		@client = Mysql2::Client.new(options)
		tables=@client.query( 'SHOW tables')
		test=false
		test=tables.each do |x|
			return true if x=="cat_box"
		end
		@client.query( 'CREATE TABLE cat_box (box_key VARCHAR(80) NOT NULL PRIMARY KEY, package BLOB)') if !test 
		return true
	end

	#Clears CatBox Table
	# @return [Boolean] True if All Goes Well
	def empty_cache
		@client.query( 'TRUNCATE TABLE cat_box;')
		return true
	end

	#Overide Array Style Operator for usefulness 
	# @param [String] index the key to look up
	# @return [Object,nil] The Object stored in that location
	def [](index)
		fetch(index)
	end

	#Overide Array Style Operator for usefulness 
	# @param [String] index the key to look up
	# @param [Object] data the object to be stored
	# @return [Object] The Object stored in that location
	def []=(index,data)
		put(index,data)
	end


	#Puts Data into CatBox
	# @param [String] index the key to look up
	# @param [Object] data the object to be stored
	# @return [Object] The Object stored in that location
	def put(index, data)
		data=data.to_msgpack
		@client.query("INSERT INTO cat_box(box_key, package) VALUES ('#{@client.escape(index.to_s)}', '#{@client.escape(data)}') ON DUPLICATE KEY UPDATE package='#{@client.escape(data)}'")
		return true
	end
	
	#Gets Data from CatBox
	# @param [String] index the key to look up
	# @return [Object,nil] The Object stored in that location
	def fetch(index)
		data=@client.query("SELECT package FROM cat_box where box_key='#{@client.escape(index.to_s)}'").first
		return nil if data.nil?
		data=data["package"]
		return MessagePack.unpack(data)
	end
end