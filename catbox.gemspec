Gem::Specification.new do |s|
  s.name        = 'CatBox'
  s.version     = '0.0.1'
  s.date        = '2012-08-22'
  s.summary     = "CatBox"
  s.description = "Simple Quick Persistant Key Value Storage in Mysql"
  s.authors     = ["Christian Stone"]
  s.email       = 'nerd31337@gmail.com'
  s.files       = ["lib/CatBox.rb"]
  s.add_dependency "msgpack"
  s.add_dependency "mysql2"
  s.homepage    =
    'https://rubygems.org/gems/CatBox'
end